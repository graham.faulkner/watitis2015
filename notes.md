# WatITis 2015 (Dec 7, 2015)

## Visualizing your data with D3
Eric Bremner, Nathan Vexler

- open data == not-publicly identifiable; accessible by design
- requires license (see UWaterloo Open Data License Agreement)
- goal: OL, RE, Open format, URI, linked data
  - economies of scale goes up; cost of using data goes down
- [D3](http://d3js.org) = "data driven documents"
  - javascript library
  - HTML, SVG and CSS
  - not bound to a framework
  - bind the data to the DOM
- worked with Student Success Office and Tutor Connect
- problem statement: which courses need more tutors? and then asked which courses are hardest and easiest
- used list of tutors from [Open Data dataset](https://github.com/uWaterloo/Datasets)
- had to create a custom API for [course enrolments](https://github.com/uWaterloo/api-documentation/blob/master/v2/terms/term_enrollment.md)
- 
- toolsets
  - D3 - d3.json() to read in JSON from Open Data API; has built-in waits and error handling
  - [alasql JS library](https://github.com/agershun/alasql) - to turn query into databas
    - focus on SQL-compliant
    - runs in client's browser
  - [D3 tip](https://github.com/Caged/d3-tip) - adds tooltips for data
  - Visualization with D3 using JSFiddle
- challenges
  - couldn't iterate through all the data quickly (or caused crash); eventually solved using alasql 
  - casting variables in JS is tricky 
  - JS is event driven, so were encountering race conditions; eventually used d3.json() to handle waits, etc.
- [project files for demo](https://github.com/ebremner17/uw_d3_open_data_tutors)
- see [C3 JS library](http://c3js.org/) - reusable chart library for D3 graphs
- they are working to integrate simple charting into WCMS

## SSL, TLS, TLAs at UWaterloo
Mike Patterson

- TLAs (three letter acronymns)
  - PKI = public key infrastructure
  - SSL
  - TLS
- CA = runs the PKI; use GlobalSign at UWaterloo
- types of certificates
  1. OrganizationSSL
  1. IntranetSSL - because CA's won't renew OrgSSLs for hidden IPs as of October 2015; requires intermediate certificate for client browsers
  1. Self-signed
  1. Other CA
- How cert issued?
  - Private key generated
  - Generate CSR (cert signing request) using the private key
  - Vetting by the CA and UWaterloo IST
  - Cert issued by CA and installed on server
- How does it work? very large prime numbers
- SSL - 2.0 deprecated in 2011; 3.0 deprecated June 2015
- TLS is SSL's replacement
  - 1.0 was essentially SSL 3.0
  - 1.1 and 1.2 - not available in all browsers
- vulnerabilities
  - POODLE - padding oracle on downgraded legacy encryption - October/December 2014 (MITM attack that downgrades from TLS to SSL)
  - Heartbleed - OpenSSL source code had bug; could be run remotely and could steal memory contents
- disable SSL 3.0 on server
- need to keep up - requires updating the operating system of servers regularly
- 761 public IPs (probably 1500 websites) on campus listening on port 443 at UWaterloo
- SSL Labs https://www.ssllabs.com/ - free test of SSL/TLS vulnerabilities
- Use ShadowServer.org for scanning UWaterloo IPs (e.g. find servers still running SSL 3.0)
  - Mike puts the report results in PostgreSQL database
  - find printers, net apps, VMWare hosts
- Mike is going to start auto nagging every few weeks
- IST has F5 load balancer to handle some SSL traffic; can't offer it as a service right now; can use reverse proxy as alternative
- TLS maturity model
  1. chaos
  1. configuration
  1. application security - e.g. avoid mixing secure/insecure content
  1. commitment - e.g. activate strict transport security
  1. robustness - e.g. certificate pinning
- goal: maturity model level 2 across campus by Dec 2016
- https://uwat.ca/ZkH for the IST SSL requirements
  
## Experiences and Uses of nginx
Patrick Matlock, Steve Weber, Ray White, Mike Nowakowski

- really fast, really simple, lots of documentation/code samples 
- very small - leave lots of room for RAM 
- has fast CGI interface - can use to bridge multiple versions of software due to multiple versions of language
- what's wrong with Apache?
  - getting long in tooth
  - each client == one thread
  - event-driven, non-blocking, asynchronous
- useful for setting up proxy server
- load balancer is a reverse proxy that distributes workloads across multiple servers
  - nginx defaults to round robin; but can also do IP-Hash (in newer versions, with IPv6) and Least Connected
- can use nginx "upstream" module for load balancing
- can use content caching to reduce proxy requests
- can do rate limiting to prevent against DOS attacks
  - spillover traffic is shown 404 (or custom response code) and logged by nginx
  - also use fail2ban module to prevent larger attacks
- nginx ~1/4 of top 1m sites, but about ~1/2 of top 10,000 sites
- i.e. nginx is especially useful when under heavy load; when load is low, little difference between Apache, IIS, etc. and nginx
- Patrick developed nginx security checklist 
- campus moving to SAML 2.0 using Shibboleth
- https://idp.uwaterloo.ca
- light comparison with Apache
  - no .htaccess 
  - no dynamic module loading - must recompile
  - proxy/reverse proxy not as mature as Apache
  - pure SSO (login/logout) is myth right now
  - easier to build CRUD API webservices
  - easier (built in) 'cache on read' for API webservices
- there is no good CAS solution for nginx right now; can use nginx to send CAS requests to Apache
- can use nginx to handle some of the tasks Apache modules currently do (e.g. SSL) to help with server load

## Examination management: an example of cross-unit cooperation
Isaac Morland

- Odyssey software is for School of Computer Science, but has now expanded to various other departments
- assigned seating for students taking exams based on number of students and number of classes available
- the system does not book rooms - they need to be made available in advance
- creates a cover page for the exam that is signed by student when taking exam
- "rush seating" == find your own seat; confusing term, so they are looking at better term that is more obvious
- Odyssey talks with various UWaterloo systems:
  - New Media Services, for printing the exam with student's name and seat number; creates official requisition including account number, contact person
  - AccessAbility Services - uploads a file to Odyssey each week to say which students are writing with them that week; New Media services sets aside exams for accessibility office
  - Registrar's office - sends email to registrar's office with exam pack, that registrar's office then forwards with other exam requests to New Media
- other users of Odyssey:
  - CEL (distance education) - use Odyssey for scheduling exams for online courses i.e. with 30-40 courses writing at one time in PAC
  - some other departments are using it too
  - CrowdMark - commercial product
    - online collaborative grading platform 
    - supply the exam template, and CrowdMark creates QR code per page
    - completed quizzes/midterms/exams are scanned in and sent to CrowdMark
    - TA's can then mark one question for all students at a time
    - have an integration API, so that can send info from Odyssey to CrowdMark
- for rooms, needed seating capacity, but also floor plans
- Odyssey imports final exam schedule via Open Data API
- have about 80 final exams in Odyssey, not including the CEL exams
- Student Portal pulls a version of Odyssey - i.e. will show the assigned seating if available
  
## Selenium Unit Testing Framework
Patrick Matlock

- [Selenium IDE plugin for Firefox](https://addons.mozilla.org/en-us/firefox/addon/selenium-ide/)
- [Selenium home page](http://www.seleniumhq.org/)
- what are your maintenance handoff procedures? do you have a way to determine whether the product is still up/stable after launch?
- Selenium is grandson of Tk/Tcl
- used for simulation of human use of web/GUI 
- can test for load, form validation, and XSS
- can export the test to various languages and then use that code to programmatically repeat the testing
- Selenium has its own programming language
- Nagios has a check_selenium plugin 
- [Zap](https://www.owasp.org/index.php/OWASP_Zed_Attack_Proxy_Project) - open source pen testing tool (available at [OWASP](https://www.owasp.org/index.php/Main_Page))
  - can integrate with Selenium to automate pen testing of a site
- OnBase - for storing court-aware documents (e.g. health services) 
  - no web services available for it
  - Patrick used Selenium and nginx with Flask
- Selenium has drivers for most operating system and browser
- Jenkins can run on top of Selenium 
- [protractor](https://angular.github.io/protractor/#/) - for testing Node.js, Angular, Silverlight
- goal: create set of baseline Selenium tests that could be activated via Jenkins


